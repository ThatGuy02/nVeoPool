# API Endpoints
## /api/miners?address={PublicKey}&includeWorkers={true|false}

> Returns aggregate stats for a miner. Hashrate measurements are in GH/s.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Boolean {includeWorkers} - Optional, default false; Whether or not to include all individual worker stats in the response

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return miner stats for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/miners?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D&includeWorkers=true&times=1,6,12,24

Reply:
[
	{
		"window": 1,
		"stats": {
			"workerList": [
				{
					"id": "w09",
					"shares": 23283,
					"avgJobDiff": 9700,
					"hashrate": 1680.55090970624,
					"invalidShares": 49,
					"lastShare": 1529721019
				}
			],
			"workers": 1,
			"shares": 23283,
			"avgJobDiff": 9700,
			"hashrate": 1680.55090970624,
			"invalidShares": 49,
			"lastShare": 1529721019
		}
	},
	{
		"window": 6,
		"stats": {
			"workerList": [
				{
					"id": "w09",
					"shares": 34742,
					"avgJobDiff": 9700,
					"hashrate": 417.9422733683674,
					"invalidShares": 73,
					"lastShare": 1529721019
				}
			],
			"workers": 1,
			"shares": 34742,
			"avgJobDiff": 9700,
			"hashrate": 417.9422733683674,
			"invalidShares": 73,
			"lastShare": 1529721019
		}
	},
	{
		"window": 12,
		"stats": {
			"workerList": [
				{
					"id": "w09",
					"shares": 34742,
					"avgJobDiff": 9700,
					"hashrate": 208.9711366841837,
					"invalidShares": 73,
					"lastShare": 1529721019
				}
			],
			"workers": 1,
			"shares": 34742,
			"avgJobDiff": 9700,
			"hashrate": 208.9711366841837,
			"invalidShares": 73,
			"lastShare": 1529721019
		}
	},
	{
		"window": 24,
		"stats": {
			"workerList": [
				{
					"id": "w03",
					"shares": 1,
					"avgJobDiff": 9700,
					"hashrate": 0.0030074713125925927,
					"invalidShares": 0,
					"lastShare": 1529652756
				},
				{
					"id": "w09",
					"shares": 203302,
					"avgJobDiff": 9700,
					"hashrate": 611.4249327926992,
					"invalidShares": 4425,
					"lastShare": 1529721019
				}
			],
			"workers": 2,
			"shares": 203303,
			"avgJobDiff": 9700,
			"hashrate": 611.4279402640118,
			"invalidShares": 4425,
			"lastShare": 1529721019
		}
	}
]
```

## /api/miners/balance?address={PublicKey}

> Returns pending and total paid balance for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

```
Request: http://localhost:8087/api/miners/balance?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D

Reply:
{
	"pendingBalance": 0.23476,
	"totalPaid": 1.2398472
}
```

## /api/miners/payment?address={PublicKey}&limit={limit}

> Returns an array of recent payment history for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Number {limit} - Optional, default 0 (unlimited); The number of payments to return.

```
Request: http://localhost:8087/api/miners/payment?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D

Reply:
[
	{
		"amount": 1.283746,
		"submittedOn": 152342342,
		"transactionFee": 0,
		"transactionId": "VEO_TX_ID"
	}
]
```

## /api/miners/top?limit={limit}&orderBy={orderBy}

> Returns top miners ranked by either accepted share count, total hashrate, invalid share count, or worker count.

> Number {limit} - Optional, default 50; The number of top miners to return. Applies individually to each time range/stat window. Set to 0 to return all miners in order.

> String {orderBy} - Optional, default 'hashrate'; The field to sort the top miners by. Use "hashrate" for total hashrate, "invalidShares" for invalid share count, "shares" for accepted share count, or "workers" for worker count.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return top miners for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/miners/top?limit=2&orderBy=workers&times=1,6,12,24

Reply:
[
	{
		"window": 1,
		"miners": [
			{
				"publicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
				"workers": 35
			},
			{
				"publicKey": "BGoPcp1g6hf//NdCNTSzJiKK45e5Ylro8v1WJuFZ01qdNt5HlWReEX1QxCcwwi5VEvNdvvC/Q2OU0XxbosjiZdg=",
				"workers": 11
			}
		]
	},
	{
		"window": 6,
		"miners": [
			{
				"publicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
				"workers": 30
			},
			{
				"publicKey": "BGoPcp1g6hf//NdCNTSzJiKK45e5Ylro8v1WJuFZ01qdNt5HlWReEX1QxCcwwi5VEvNdvvC/Q2OU0XxbosjiZdg=",
				"workers": 9
			}
		]
	},
	{
		"window": 12,
		"miners": [
			{
				"publicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
				"workers": 25
			},
			{
				"publicKey": "BGoPcp1g6hf//NdCNTSzJiKK45e5Ylro8v1WJuFZ01qdNt5HlWReEX1QxCcwwi5VEvNdvvC/Q2OU0XxbosjiZdg=",
				"workers": 7
			}
		]
	},
	{
		"window": 24,
		"miners": [
			{
				"publicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
				"workers": 20
			},
			{
				"publicKey": "BGoPcp1g6hf//NdCNTSzJiKK45e5Ylro8v1WJuFZ01qdNt5HlWReEX1QxCcwwi5VEvNdvvC/Q2OU0XxbosjiZdg=",
				"workers": 5
			}
		]
	}
]
```

## /api/miners/workers?address={PublicKey}

> Returns an array of all worker ids for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return workers for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/miners/workers?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D&times=1,6,12,24

Reply:
[
	{
		"window": 1,
		"workers": [
			"w09"
		]
	},
	{
		"window": 6,
		"workers": [
			"w09"
		]
	},
	{
		"window": 12,
		"workers": [
			"w09"
		]
	},
	{
		"window": 24,
		"workers": [
			"w03",
			"w09"
		]
	}
]
```

## /api/miners/workers?address={PublicKey}&workerId={WorkerId}

> Returns individual worker stats for a miner. Hashrate measurements are in GH/s.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> String {WorkerId} - Required; The worker id.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return stats for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/miners/workers?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D&workerId=rig00&times=1,6,12,24

Reply:
[
	{
		"window": 1,
		"stats": {
			"shares": 23959,
			"avgJobDiff": 9700,
			"hashrate": 1729.344124281742,
			"invalidShares": 49,
			"lastShare": 1529720488
		}
	},
	{
		"window": 6,
		"stats": {
			"shares": 31988,
			"avgJobDiff": 9700,
			"hashrate": 384.8119693888474,
			"invalidShares": 67,
			"lastShare": 1529720488
		}
	},
	{
		"window": 12,
		"stats": {
			"shares": 31988,
			"avgJobDiff": 9700,
			"hashrate": 192.4059846944237,
			"invalidShares": 67,
			"lastShare": 1529720488
		}
	},
	{
		"window": 24,
		"stats": {
			"shares": 200021,
			"avgJobDiff": 9700,
			"hashrate": 601.557419416083,
			"invalidShares": 4419,
			"lastShare": 1529720351
		}
	}
]
```

## /api/pool/blocks?limit={limit}

> Returns an array of the most recent pool blocks found.

> Number {limit} - Optional, default 50; The number of blocks to return.

```
Request: http://localhost:8087/api/pool/blocks

Reply:
[
	{
		"blockHeight": 18100,
		"isPaid": false,
		"minerPublicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
		"status": "Unconfirmed (Accepted)",
		"statusId": 3,
		"submittedOn": 1522224123
	},
	{
		"blockHeight": 18095,
		"isPaid": false,
		"minerPublicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
		"status": "Unconfirmed (Rejected)",
		"statusId": 4,
		"submittedOn": 1522222122
	},
	{
		"blockHeight": 18085,
		"isPaid": false,
		"minerPublicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
		"status": "Orphaned",
		"statusId": 2,
		"submittedOn": 1522220121
	},
	{
		"blockHeight": 18080,
		"isPaid": true,
		"minerPublicKey": "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=",
		"status": "Confirmed",
		"statusId": 1,
		"submittedOn": 1522204736
	}
]

StatusId Enum:
1 = Confirmed
2 = Orphaned
3 = Unconfirmed (Accepted)
4 = Unconfirmed (Rejected)
```

## /api/pool/stats

> Returns aggregate pool miner stats. Hashrate measurements are in GH/s.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return stats for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/pool/stats?times=1,6,12,24

Reply:
[
	{
		"window": 1,
		"stats": {
			"miners": 198,
			"workers": 198,
			"shares": 132852,
			"avgJobDiff": 9700,
			"hashrate": 9589.165891693227,
			"invalidShares": 367,
			"lastShare": 1529720488
		}
	},
	{
		"window": 6,
		"stats": {
			"miners": 198,
			"workers": 198,
			"shares": 173840,
			"avgJobDiff": 9700,
			"hashrate": 2091.2752519243854,
			"invalidShares": 479,
			"lastShare": 1529720488
		}
	},
	{
		"window": 12,
		"stats": {
			"miners": 198,
			"workers": 198,
			"shares": 173840,
			"avgJobDiff": 9700,
			"hashrate": 1045.6376259621927,
			"invalidShares": 479,
			"lastShare": 1529720488
		}
	},
	{
		"window": 24,
		"stats": {
			"miners": 200,
			"workers": 201,
			"shares": 1480624,
			"avgJobDiff": 9700,
			"hashrate": 4452.934204736095,
			"invalidShares": 17033,
			"lastShare": 1529720488
		}
	}
]
```

## /api/series/miners/hashrate?address={PublicKey}&start={start}&end={end}

> Returns time-series for average miner hashrate.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/miners/hashrate?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"hashrate": 129.5266692755911,
				"t": 1533800652
			},
			{
				"hashrate": 99.59074722247111,
				"t": 1533800580
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"hashrate": 21.58777821259852,
				"t": 1533800652
			},
			{
				"hashrate": 16.598457870411853,
				"t": 1533800580
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"hashrate": 10.79388910629926,
				"t": 1533800652
			},
			{
				"hashrate": 8.299228935205926,
				"t": 1533800580
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"hashrate": 5.39694455314963,
				"t": 1533800652
			},
			{
				"hashrate": 4.149614467602963,
				"t": 1533800580
			}
		]
	}
]
```

## /api/series/miners/hashrate?address={PublicKey}&workerId={WorkerId}&start={start}&end={end}

> Returns time-series for average worker hashrate.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> String {WorkerId} - Required; The worker id.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/miners/hashrate?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D&workerId=rig00

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"hashrate": 29.5266692755911,
				"t": 1533800652
			},
			{
				"hashrate": 9.59074722247111,
				"t": 1533800580
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"hashrate": 1.58777821259852,
				"t": 1533800652
			},
			{
				"hashrate": 6.598457870411853,
				"t": 1533800580
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"hashrate": 1.79388910629926,
				"t": 1533800652
			},
			{
				"hashrate": 0.299228935205926,
				"t": 1533800580
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"hashrate": 5.39694455314963,
				"t": 1533800652
			},
			{
				"hashrate": 4.149614467602963,
				"t": 1533800580
			}
		]
	}
]
```

## /api/series/miners/shares?address={PublicKey}&start={start}&end={end}

> Returns time-series for miner accepted & invalid share totals.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/miners/shares?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	}
]
```

## /api/series/miners/shares?address={PublicKey}&workerId={WorkerId}&start={start}&end={end}

> Returns time-series for worker accepted & invalid share totals.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> String {WorkerId} - Required; The worker id.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/shares/hashrate?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D&workerId=rig00

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"shares": 339,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 313,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"shares": 398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 395,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"shares": 3398,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	}
]
```

## /api/series/miners/workers?address={PublicKey}&start={start}&end={end}

> Returns time-series for miner worker count.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/miners/workers?address=BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP%2BLVW%2BSubt3EKO%2FwY5WVBfHBY%3D

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"workers": 2,
				"t": 1533870411
			},
			{
				"workers": 2,
				"t": 1533870342
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"workers": 3,
				"t": 1533870411
			},
			{
				"workers": 3,
				"t": 1533870342
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"workers": 4,
				"t": 1533870411
			},
			{
				"workers": 4,
				"t": 1533870342
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"workers": 5,
				"t": 1533870411
			},
			{
				"workers": 5,
				"t": 1533870342
			}
		]
	}
]
```

## /api/series/pool/hashrate?start={start}&end={end}

> Returns time-series for average pool hashrate.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/pool/hashrate

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"hashrate": 1229.5266692755911,
				"t": 1533800652
			},
			{
				"hashrate": 994.59074722247111,
				"t": 1533800580
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"hashrate": 213.58777821259852,
				"t": 1533800652
			},
			{
				"hashrate": 165.598457870411853,
				"t": 1533800580
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"hashrate": 102.79388910629926,
				"t": 1533800652
			},
			{
				"hashrate": 83.299228935205926,
				"t": 1533800580
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"hashrate": 58.39694455314963,
				"t": 1533800652
			},
			{
				"hashrate": 46.149614467602963,
				"t": 1533800580
			}
		]
	}
]
```

## /api/series/pool/shares?start={start}&end={end}

> Returns time-series for pool accepted & invalid shares.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/pool/shares

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"shares": 3349,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3153,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"shares": 3978,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3959,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"shares": 339568,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 313344,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"shares": 3394568,
				"invalidShares": 0,
				"t": 1533801149
			},
			{
				"shares": 3345134,
				"invalidShares": 0,
				"t": 1533801078
			}
		]
	}
]
```

## /api/series/pool/workers?start={start}&end={end}

> Returns time-series for pool worker count.

> Number {end} - Optional, default 0; The latest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> Number {start} - Optional, default 0; The earliest timestamp to include in the time-series. Applies individually to each time range/stat window. Negative values are supported and will be interpreted as "seconds in the past", e.g. -5 is 5 seconds ago. 0 will be ignored.

> List {times} - Optional, default 1,6,12,24; Which time ranges (measured in hours) to return time-series for. Duplicate time ranges and non-supported values (i.e. anything other than 1, 6, 12, or 24) will be safely ignored.

```
Request: http://localhost:8087/api/series/pool/workers

Reply:
[
	{
		"window": 1,
		"series": [
			{
				"miners": 128,
				"workers": 185,
				"t": 1533801434
			},
			{
				"miners": 126,
				"workers": 182,
				"t": 1533801362
			}
		]
	},
	{
		"window": 6,
		"series": [
			{
				"miners": 128,
				"workers": 185,
				"t": 1533801434
			},
			{
				"miners": 126,
				"workers": 182,
				"t": 1533801362
			}
		]
	},
	{
		"window": 12,
		"series": [
			{
				"miners": 202,
				"workers": 309,
				"t": 1533801434
			},
			{
				"miners": 200,
				"workers": 306,
				"t": 1533801362
			}
		]
	},
	{
		"window": 24,
		"series": [
			{
				"miners": 222,
				"workers": 361,
				"t": 1533801434
			},
			{
				"miners": 220,
				"workers": 358,
				"t": 1533801362
			}
		]
	}
]
```

## /api/admin/servers

> Returns the registered servers on all cache adapters.

```
Request: http://localhost:8087/api/admin/servers

Reply:
{
	"api": [
		"api-master-00"
	],
	"pool": [
		"pool-master-00",
		"pool-slave-00"
	]
}
```

## /api/admin/servers/{ServerId}

> Returns brief status information about the server.

> String {ServerId} - Required; The id of the server.

```
Request: http://localhost:8087/api/admin/servers/pool-master-00

Reply:
{
	"category": "Pool",
	"heartbeat": 1527826327,
	"registeredOn": 1530245595
	"type": "Master",
	"uptime": 125,
	"version": "1.0.0.0"
}
```

## /api/admin/servers/{ServerId}/config

> Returns an array of available configuration sections for the server. Sections containing potentially sensitive information (e.g. usernames, passwords, access tokens) are not available.

> String {ServerId} - Required; The id of the server.

```
Request: http://localhost:8087/api/admin/servers/pool-master-00/config

Reply:
[
	"Clustering",
	"Logging",
	"NodeRpc",
	"Payment",
	"ServiceTimers",
	"VarDiff",
	"WorkProtocol"
]
```

## /api/admin/servers/{ServerId}/config/{ConfigSection}

> Returns the configuration section for the server.

> String {ServerId} - Required; The id of the server.

> String {ConfigSection} - Required; The configuration section name.

```
Request: http://localhost:8087/api/admin/servers/pool-master-00/config/Payment

Reply:
{
	"batchSendDelay": 10,
	"batchSendSize": 3,
	"blockMaturityDepth": 10,
	"devDonation": 0.1,
	"groupPayments": true,
	"individualSendDelay": 4000,
	"minimumPayoutThreshold": 0.1,
	"paymentMaturityDepth": 10,
	"poolFee": 1.99,
	"poolPaysTransactionFee": true,
	"roundLength": 10,
	"transactionFee": 0
}
```

## /api/admin/servers/{ServerId}/services

> Returns an array of all registered services on the server.

> String {ServerId} - Required; The id of the server.

```
Request: http://localhost:8087/api/admin/servers/pool-master-00/services

Reply:
[
	"BanManagementService",
	"BlockCandidateProcessorService",
	"CacheCleanupService",
	"InvalidShareCleanupService",
	"InvalidShareStorageService_pool-master-00",
	"NodeFailoverService",
	"PaymentSenderService",
	"ShareArchiverService",
	"ShareStorageService_pool-master-00",
	"WorkRetrieverService"
]
```

## /api/admin/servers/{ServerId}/services/{ServiceId}

> Returns status information for the service on the server. See [Service Runtime Data](service-runtime-data.md) for more information.

> String {ServerId} - Required; The id of the server.

> String {ServiceId} - Required; The id of the service.

```
Request: http://localhost:8087/api/admin/servers/pool-master-00/services/PaymentSenderService

Reply:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"batchCount": 6,
		"currentBatch": 1,
		"errorCount": 0,
		"paymentCount": 16,
		"processedCount": 2,
		"remainingInBatch": 1,
		"remainingTotal": 14,
		"sucessfullCount": 2
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```