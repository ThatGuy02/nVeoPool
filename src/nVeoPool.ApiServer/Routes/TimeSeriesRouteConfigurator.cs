﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using Microsoft.AspNetCore.Routing;
using nVeoPool.ApiServer.RequestHandlers;

namespace nVeoPool.ApiServer.Routes {
	internal class TimeSeriesRouteConfigurator : ITimeSeriesRouteConfigurator {
		public TimeSeriesRouteConfigurator(ITimeSeriesRouteRequestHandler timeSeriesRouteRequestHandler) {
			_timeSeriesRouteRequestHandler = timeSeriesRouteRequestHandler;
		}

		#region ITimeSeriesRouteConfigurator
		public void Configure(IRouteBuilder routeBuilder) {
			routeBuilder.MapGet("api/series/miners/hashrate", _timeSeriesRouteRequestHandler.HandleGetHashrateSeriesForMinerAsync);
			routeBuilder.MapGet("api/series/miners/shares", _timeSeriesRouteRequestHandler.HandleGetShareSeriesForMinerAsync);
			routeBuilder.MapGet("api/series/miners/workers", _timeSeriesRouteRequestHandler.HandleGetWorkerSeriesForMinerAsync);
			routeBuilder.MapGet("api/series/pool/hashrate", _timeSeriesRouteRequestHandler.HandleGetHashrateSeriesForPoolAsync);
			routeBuilder.MapGet("api/series/pool/shares", _timeSeriesRouteRequestHandler.HandleGetShareSeriesForPoolAsync);
			routeBuilder.MapGet("api/series/pool/workers", _timeSeriesRouteRequestHandler.HandleGetWorkerSeriesForPoolAsync);
		}
		#endregion

		private readonly ITimeSeriesRouteRequestHandler _timeSeriesRouteRequestHandler;
	}
}
