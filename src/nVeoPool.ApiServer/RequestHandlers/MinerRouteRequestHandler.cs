#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class MinerRouteRequestHandler : RequestHandlerBase, IMinerRouteRequestHandler {
		public MinerRouteRequestHandler(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger<MinerRouteRequestHandler> logger, IRepositoryCacheAdapter repositoryCache) : base(apiServerConfiguration, cache, logger) {
			_repositoryCache = repositoryCache;
		}

		#region IMinerRouteRequestHandler
		public Task HandleGetMinerAggregateStatsAsync(HttpContext context, String minerPublicKey) {
			return HandleResponseAsync(context, async () => {
				var includeWorkers = context.Request.Query.TryGetValue("includeWorkers", out var includeWorkersObject) && bool.TryParse(includeWorkersObject.ToString(), out var includeWorkersValue) && includeWorkersValue;
				var minerStats = await _repositoryCache.GetStatsForMinerAsync(minerPublicKey, ParseTimesQueryToSeconds(context.Request.Query));

				if (minerStats != null) {
					return (null, JsonConvert.SerializeObject(minerStats.Select(m => {
						if (!includeWorkers) {
							m.IndividualWorkerStats = null;
						}

						return new { Window = m.WindowLengthSeconds / 3600, Stats = m };
					}), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetMinerAggregateStatsAsync));
		}

		public Task HandleGetMinerBalanceInfoAsync(HttpContext context, String minerPublicKey) {
			return HandleResponseAsync(context, async () => {
				var minerBalance = await _repositoryCache.GetBalanceForMinerAsync(minerPublicKey);

				if (minerBalance != null) {
					return (null, JsonConvert.SerializeObject(minerBalance, _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetMinerBalanceInfoAsync));
		}

		public Task HandleGetMinerPaymentsAsync(HttpContext context, String minerPublicKey) {
			return HandleResponseAsync(context, async () => {
				var limit = ParseLimitOrDefault(context.Request.Query, DEFAULT_MINER_PAYMENT_LIMIIT);
				var minerPayments = await _repositoryCache.GetAllPaymentsForMinerAsync(minerPublicKey, _apiServerConfiguration.QueryConfiguration.MaximumMinerPayments);
				if (limit > 0) {
					minerPayments = minerPayments.Take(limit);
				}

				return (null, JsonConvert.SerializeObject(minerPayments, _jsonSerializerSettings));
			}, nameof(HandleGetMinerPaymentsAsync));
		}

		public Task HandleGetTopMinersAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var limit = ParseLimitOrDefault(context.Request.Query, DEFAULT_TOP_MINER_LIMIT);
				var orderByField = (context.Request.Query.TryGetValue("orderBy", out var orderByObj) ? orderByObj.ToString() : String.Empty).Trim().ToLower();
				var orderBy = _allowedOrderByFields.Contains(orderByField) ? orderByField : DEFAULT_TOP_MINER_ORDER_BY;
				var topMiners = await _repositoryCache.GetTopMinersByFieldAsync(orderBy, ParseTimesQueryToSeconds(context.Request.Query), _apiServerConfiguration.QueryConfiguration.MaximumTopMiners);

				return (null, JsonConvert.SerializeObject(topMiners.GroupBy(m => m.WindowLengthSeconds).Select(kv => new { Window = kv.Key / 3600, Miners = limit > 0 ? kv.Take(limit) : kv }), _jsonSerializerSettings));
			}, nameof(HandleGetTopMinersAsync));
		}

		public Task HandleGetWorkerAggregateStatsAsync(HttpContext context, String minerPublicKey, String workerId) {
			return HandleResponseAsync(context, async () => {
				var workerStats = await _repositoryCache.GetStatsForWorkerForMinerAsync(minerPublicKey, workerId, ParseTimesQueryToSeconds(context.Request.Query));

				if (workerStats != null) {
					return (null, JsonConvert.SerializeObject(workerStats.Select(w => new { Window = w.WindowLengthSeconds / 3600, Stats = w }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetWorkerAggregateStatsAsync));
		}

		public Task HandleGetWorkerListForMinerAsync(HttpContext context, String minerPublicKey) {
			return HandleResponseAsync(context, async () => {
				var workerList = await _repositoryCache.GetWorkerListForMinerAsync(minerPublicKey, ParseTimesQueryToSeconds(context.Request.Query));

				if (workerList != null) {
					return (null, JsonConvert.SerializeObject(workerList.Where(w => !String.IsNullOrWhiteSpace(w.WorkerId)).GroupBy(w => w.WindowLengthSeconds).Select(kv => new { Window = kv.Key / 3600, Workers = kv.Select(wk => wk.WorkerId) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetWorkerListForMinerAsync));
		}
		#endregion

		private const int DEFAULT_MINER_PAYMENT_LIMIIT = 0;
		private const int DEFAULT_TOP_MINER_LIMIT = 50;
		private const String DEFAULT_TOP_MINER_ORDER_BY = "hashrate";
		private static readonly ISet<String> _allowedOrderByFields = new HashSet<String> { "hashrate", "invalidshares", "shares", "workers" };
		private readonly IRepositoryCacheAdapter _repositoryCache;
	}
}
