#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal abstract class RequestHandlerBase {
		protected RequestHandlerBase(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger logger) {
			_apiServerConfiguration = apiServerConfiguration;
			_cache = cache;
			_logger = logger;
		}

		protected async Task HandleResponseAsync(HttpContext context, Func<Task<(int? StatusCode, String ResponseJson)>> responseGenerator, String actionName) {
			(int? StatusCode, String ResponseJson) response = (200, String.Empty);

			try {
				response = await responseGenerator();

				if (!String.IsNullOrWhiteSpace(response.ResponseJson)) {
					context.Response.ContentLength = response.ResponseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = response.StatusCode.Value;
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", actionName);
			}

			await context.Response.WriteAsync(response.ResponseJson);
		}

		protected int ParseIntOrDefault(IQueryCollection requestQuery, String name, int defaultValue) {
			return requestQuery.TryGetValue(name, out var intObj) && int.TryParse(intObj.ToString(), out var intVal) ? intVal : defaultValue;
		}

		protected int ParseLimitOrDefault(IQueryCollection requestQuery, int defaultValue) {
			return requestQuery.TryGetValue("limit", out var limitObj) && int.TryParse(limitObj.ToString(), out var limitInt) && limitInt >= 0 ? limitInt : defaultValue;
		}

		protected IEnumerable<uint> ParseTimesQueryToSeconds(IQueryCollection requestQuery) {
			var times = requestQuery.TryGetValue("times", out var timesObject) ? timesObject.ToString().Split(',').Select(t => uint.TryParse(t, out var timeInt) ? timeInt : 0).Where(t => _apiServerConfiguration.QueryConfiguration.QueryableHourRanges.Contains(t)) : _apiServerConfiguration.QueryConfiguration.QueryableHourRanges;

			return times.Distinct().Select(t => t * 3600);
		}

		protected readonly IApiServerConfiguration _apiServerConfiguration;
		protected readonly IDistributedCacheAdapter _cache;
		protected static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
			Converters = new List<JsonConverter> { new StringEnumConverter() },
			NullValueHandling = NullValueHandling.Ignore
		};
		protected readonly ILogger _logger;
	}
}
