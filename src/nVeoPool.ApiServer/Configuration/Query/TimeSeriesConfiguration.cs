#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.ApiServer.Configuration.Query {
	internal class TimeSeriesConfiguration {
		[DefaultValue(DEFAULT_MAXIMUM_QUERY_AGE_SECONDS)]
		[JsonProperty(PropertyName = "MaximumQueryAge")]
		public int MaximumQueryAgeSeconds { get; set; } = DEFAULT_MAXIMUM_QUERY_AGE_SECONDS;

		[DefaultValue(DEFAULT_MAXIMUM_STORAGE_AGE_SECONDS)]
		[JsonProperty(PropertyName = "MaximumStorageDuration")]
		public int MaximumStorageAgeSeconds { get; set; } = DEFAULT_MAXIMUM_STORAGE_AGE_SECONDS;

		[DefaultValue(DEFAULT_MINIMUM_CAPTURE_INTERVAL_SECONDS)]
		[JsonProperty(PropertyName = "MinimumCaptureInterval")]
		public int MinimumCaptureIntervalSeconds { get; set; } = DEFAULT_MINIMUM_CAPTURE_INTERVAL_SECONDS;

		private const int DEFAULT_MAXIMUM_QUERY_AGE_SECONDS = 24*60*60;
		private const int DEFAULT_MAXIMUM_STORAGE_AGE_SECONDS = 48*60*60;
		private const int DEFAULT_MINIMUM_CAPTURE_INTERVAL_SECONDS = 1*60;
	}
}