#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.ApiServer.Services.RuntimeData;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;

namespace nVeoPool.ApiServer.Services {
	internal class DataCacherService : ReportingBackgroundServiceBase {
		public DataCacherService(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ICacheConnectionFactory cacheConnectionFactory, IReaderWriterLock globalStatisticsLock, ILogger<DataCacherService> logger, IStatsRepository statsRepository) : base(cache, apiServerConfiguration.ClusteringConfiguration, logger, apiServerConfiguration.ServicesConfiguration.DataCacherServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(DataCacherService)) {
			_apiServerConfiguration = apiServerConfiguration;
			_cacheConnectionFactory = cacheConnectionFactory;
			_globalStatisticsLock = globalStatisticsLock;
			_lockAcquisitionDelaySeconds = _apiServerConfiguration.ServicesConfiguration.DataCacherSynchronizationServiceUpdateIntervalSeconds * 3;
			_runtimeData = new DataCacherData();
			_statsRepository = statsRepository;
			_timeSeriesEnabled = _apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MinimumCaptureIntervalSeconds > 0;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());
			var lastTimeSeriesUpdateOn = 0L;

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var currentTime = DateTime.UtcNow;
					var enableSeriesUpdate = _timeSeriesEnabled && ((new DateTimeOffset(currentTime).ToUnixTimeSeconds() - lastTimeSeriesUpdateOn) >= _apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MinimumCaptureIntervalSeconds);
					var cutoffTime = enableSeriesUpdate ? (long?)new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumStorageAgeSeconds)).ToUnixTimeSeconds() : null;

					if (await _globalStatisticsLock.TryEnterWriteLockAsync(-1, cancellationToken)) {
						try {
							await _cacheConnectionFactory.GetSubscriber().PublishSimpleAsync(_apiServerConfiguration.ChannelConfiguration.StatisticsUpdateLockAcquired, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());

							_logger.LogDebug("{BackgroundService} acquired lock and notified slaves, pausing for {LockAcquisitionDelay} seconds", _serviceName, _lockAcquisitionDelaySeconds);

							await Task.Delay(TimeSpan.FromSeconds(_lockAcquisitionDelaySeconds), cancellationToken);
						} catch (Exception e) {
							_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to notify slaves of lock acquisition", _serviceName);
						}

						try {
							var updateResults = await Task.WhenAll(_apiServerConfiguration.QueryConfiguration.QueryableHourRanges.Select(t => RefreshAggregateStatsWindowAsync(t, cutoffTime)));

							_runtimeData.AggregateStatsUpdateResults = updateResults.Select(u => new DataCacherAggregateStatsUpdateResult {
								UpdateDuration = u.UpdateDuration,
								WindowLength = u.WindowLength
							}).ToList();

							lastTimeSeriesUpdateOn = enableSeriesUpdate ? new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds() : lastTimeSeriesUpdateOn;
							_runtimeData.LastSeriesRefreshOn = lastTimeSeriesUpdateOn;
						} finally {
							_globalStatisticsLock.ExitWriteLock();
						}

						try {
							await _cacheConnectionFactory.GetSubscriber().PublishSimpleAsync(_apiServerConfiguration.ChannelConfiguration.StatisticsUpdateLockReleased, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());

							_logger.LogDebug("{BackgroundService} successfully notified slaves of lock release, pausing for {LockReleaseDelay} seconds", _serviceName, _lockAcquisitionDelaySeconds);

							await Task.Delay(TimeSpan.FromSeconds(_lockAcquisitionDelaySeconds), cancellationToken);
						} catch (Exception e) {
							_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to notify slaves of lock release", _serviceName);
						}
					} else {
						_logger.LogWarning("Unable to acquire write lock for updating cache");
					}

					await UpdateServiceRuntimeDataAsync(_runtimeData);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync(false);
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private async Task<(uint WindowLength, long UpdateDuration)> RefreshAggregateStatsWindowAsync(uint windowLength, long? cutoffSeriesTime) {
			try {
				var windowLengthSeconds = windowLength * 3600;
				_logger.LogInformation("{BackgroundService} starting refresh of aggregate stats for window length {WindowLength} hour ({WindowLengthSeconds} seconds)", _serviceName, windowLength, windowLengthSeconds);
				if (cutoffSeriesTime.HasValue) {
					_logger.LogInformation("{BackgroundService} including time series updates for window length {WindowLength} hour with cutoff at {CutoffSeriesTime}", _serviceName, windowLength, cutoffSeriesTime.Value);
				}

				var startTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

				await _statsRepository.RefreshAggregateStatsAsync(new [] { windowLengthSeconds }, cutoffSeriesTime);

				var duration = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds() - startTime;

				_logger.LogInformation("{BackgroundService} successfully refreshed aggregate stats for window length {WindowLength} hour in {UpdateDuration} seconds", _serviceName, windowLength, duration);

				return (windowLength, duration);
			} catch (Exception e) {
				_logger.LogError(e, "Exception in {BackgroundService} while refreshing aggregate stats for window length {WindowLength}", _serviceName, windowLength);

				return (windowLength, -1);
			}
		}

		private readonly IApiServerConfiguration _apiServerConfiguration;
		private readonly ICacheConnectionFactory _cacheConnectionFactory;
		private readonly IReaderWriterLock _globalStatisticsLock;
		private readonly uint _lockAcquisitionDelaySeconds;
		private readonly DataCacherData _runtimeData;
		private readonly IStatsRepository _statsRepository;
		private readonly bool _timeSeriesEnabled;
	}
}
