﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using nVeoPool.Common.Extensions;

namespace nVeoPool.Common.Rpc {
	public class ExternalApiClient : ApiClientBase, IExternalApiClient {
		public ExternalApiClient(Uri nodeAddress) : base(nodeAddress) {
		}

		#region IExternalApiClient
		public String HostName => _nodeAddress.Host;

		public Task<String> GetAccountAsync(String publicKey, CancellationToken cancellationToken = new CancellationToken()) {
			return GetNodeResponseAsync($"[\"account\",\"{publicKey}\"]", cancellationToken);
		}

		public Task<String> GetBlockAsync(int blockHeight, CancellationToken cancellationToken = new CancellationToken()) {
			return GetNodeResponseAsync($"[\"block\",{blockHeight}]", cancellationToken);
		}

		public Task<String> GetCurrentHeightAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("height", cancellationToken);
		}

		public async Task<IEnumerable<(String IpAddress, int Port)>> GetPeersAsync(CancellationToken cancellationToken = new CancellationToken()) {
			var peersString = await GetSimpleCommandResponseAsync("peers", cancellationToken);

			return peersString.ToJArray()[1].Skip(1).Select(p => ($"{p[1][1]}.{p[1][2]}.{p[1][3]}.{p[1][4]}", p[2].Value<int>()));
		}

		public Task<String> GetTopAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("top", cancellationToken);
		}
		#endregion
	}
}
