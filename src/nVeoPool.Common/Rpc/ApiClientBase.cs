﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace nVeoPool.Common.Rpc {
    public abstract class ApiClientBase {
		protected ApiClientBase(Uri nodeAddress) {
			_nodeAddress = nodeAddress;
		}

		protected Task<String> GetNodeResponseAsync(String requestData, CancellationToken cancellationToken = new CancellationToken()) {
			return GetNodeResponseAsync(requestData, null, cancellationToken);
		}

		protected async Task<String> GetNodeResponseAsync(String requestData, String contentType, CancellationToken cancellationToken = new CancellationToken()) {
			using (var httpClient = new HttpClient()) {
				var requestContent = !String.IsNullOrWhiteSpace(contentType) ? new StringContent(requestData, Encoding.UTF8, contentType) : new StringContent(requestData);
				var response = await httpClient.PostAsync(_nodeAddress, requestContent, cancellationToken);
				response.EnsureSuccessStatusCode();

				return await response.Content.ReadAsStringAsync();
			}
		}

		protected Task<String> GetSimpleCommandResponseAsync(String command, CancellationToken cancellationToken = new CancellationToken()) {
			return GetNodeResponseAsync($"[\"{command}\"]", cancellationToken);
		}

		protected readonly Uri _nodeAddress;
	}
}
