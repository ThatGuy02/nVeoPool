﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Common {
	public class AddressParser : IAddressParser {
		#region IAddressParser
		public String FormatMinerAddressForCacheKey(String minerAddress) {
			return minerAddress.Trim().ToLower();
		}

		public (String PublicKey, String WorkerId) ParseMinerAddress(String minerAddress) {
			var dotIndex = minerAddress.IndexOf(".", StringComparison.InvariantCultureIgnoreCase);
			if (dotIndex > 0) {
				return (minerAddress.Substring(0, dotIndex).Trim(), minerAddress.Substring(dotIndex + 1).Trim().ToLower());
			}

			return (minerAddress.Trim(), String.Empty);
		}

		public bool ValidateMinerAddress(String minerAddress) {
			if (String.IsNullOrWhiteSpace(minerAddress)) {
				return false;
			}

			var minerPublicKey = ParseMinerAddress(minerAddress).PublicKey;

			if (minerPublicKey.Length != 88) {
				return false;
			}

			try {
				var unused = Convert.FromBase64String(minerPublicKey);
			} catch {
				return false;
			}

			return true;
		}
		#endregion
	}
}
