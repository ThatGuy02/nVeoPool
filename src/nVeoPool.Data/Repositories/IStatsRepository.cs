#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public interface IStatsRepository {
		Task<IEnumerable<AggregatePeriodPoolStats>> GetStatsForPoolAsync(IEnumerable<uint> windowLengths);

		Task<IEnumerable<AggregatePeriodMinerStats>> GetStatsForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths);

		Task<IEnumerable<IndividualPeriodWorkerStats>> GetStatsForWorkerForMinerAsync(String minerPublicKey, String workerId, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForMinerHashrateAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForMinerSharesAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesWorkers>> GetTimeSeriesForMinerWorkersAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForPoolHashrateAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForPoolSharesAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesPoolWorkers>> GetTimeSeriesForPoolWorkersAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForWorkerHashrateAsync(String minerPublicKey, String minerWorkerId, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForWorkerSharesAsync(String minerPublicKey, String minerWorkerId, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges);

		Task<IEnumerable<TopMiner>> GetTopMinersByAcceptedSharesAsync(IEnumerable<uint> windowLengths, int limit);

		Task<IEnumerable<TopMiner>> GetTopMinersByHashrateAsync(IEnumerable<uint> windowLengths, int limit);

		Task<IEnumerable<TopMiner>> GetTopMinersByInvalidSharesAsync(IEnumerable<uint> windowLengths, int limit);

		Task<IEnumerable<TopMiner>> GetTopMinersByWorkerCountAsync(IEnumerable<uint> windowLengths, int limit);

		Task<IEnumerable<IndividualPeriodWorkerStats>> GetWorkerListForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths);

		Task RefreshAggregateStatsAsync(IEnumerable<uint> windowLengths, long? cutoffSeriesTime);
	}
}