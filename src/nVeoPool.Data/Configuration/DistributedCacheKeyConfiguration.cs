#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Data.Configuration {
	public class DistributedCacheKeyConfiguration : IDistributedCacheKeyConfiguration {
		#region IDistributedCacheKeyConfiguration
		public String ActiveWorkersKey(int port) => $"{KEY_PREFIX}workers:{port}:active";

		public String GenericObjectKey(String key) => $"{KEY_PREFIX}obj:{key}";

		public String MinerRecentSharesKey(String minerPublicKey) => $"{KEY_PREFIX}miners:{minerPublicKey}:shares";

		public String MinerRecentSharesOldKey(String minerPublicKey) => $"{KEY_PREFIX}miners:{minerPublicKey}:sharesOld";

		public String MinerTransactionHistoryKey(String minerPublicKey) => $"{KEY_PREFIX}miners:{minerPublicKey}:transactions";

		public String NodeMiningDataKey => $"{KEY_PREFIX}miningData";

		public String PoolPublicKeyWhitelistKey => $"{KEY_PREFIX}pool:allowedPubKeys";

		public String PoolPublicKeyWhitelistKeyTempEmpty => $"{KEY_PREFIX}pool:apkEmpty";
		
		public String PoolPublicKeyWhitelistKeyTempNew => $"{KEY_PREFIX}pool:apkNew";

		public String RegisteredServersKey(String serverCategory) => $"{KEY_PREFIX}admin:{serverCategory}:svrs";

		public String RegisteredServicesOnCacheKey => $"{KEY_PREFIX}admin:svcs";

		public String RegisteredServicesOnServerKey(String serverId) => $"{KEY_PREFIX}admin:{serverId}:svcs";

		public String ServerConfigSections(String serverId) => $"{KEY_PREFIX}admin:{serverId}:cfg";

		public String ServerStatusKey(String serverId) => $"{KEY_PREFIX}admin:{serverId}:status";

		public String ServiceStatusKey(String serverId, String serviceName) => $"{KEY_PREFIX}admin:{serverId}:{serviceName}:status";

		public String WorkerRecentShareIntervalsKey(String workerAddress, int port) => $"{KEY_PREFIX}workers:{port}:{workerAddress}:shareIntervals";
		
		public String WorkerStatsKey(String workerAddress, int port) => $"{KEY_PREFIX}workers:{port}:{workerAddress}:stats";
		#endregion

		private const String KEY_PREFIX = "nvp:";
	}
}
