#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Data.Models {
	[Serializable]
	public class BlockCandidate {
		public int BlockHeight { get; set; }

		public bool IsPaid { get; set; }

		public String MinerPublicKey { get; set; }

		public String Nonce { get; set; }

		public BlockCandidateStatusType Status => (BlockCandidateStatusType)StatusId;

		public int StatusId { get; set; }

		public long SubmittedOn { get; set; }
	}
}