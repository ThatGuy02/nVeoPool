#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public interface IDistributedCacheAdapter {
		void ClearWorkerCurrentJobDifficultyImmediate(String minerAddress, int port);
		
		Task<bool> ClearWorkerInfoAsync(String workerAddress, int port);

		Task<bool> ClearWorkerRecentShareIntervalsAsync(String workerAddress, int port, long maximumListLength = 0);

		bool DeleteNodeMiningDataImmediate();

		Task<IEnumerable<String>> GetActiveWorkersAsync(int port);

		Task<(String BlockHash, int BlockDifficulty, int CurrentHeight, int FieldZero, ulong NodeClientId)> GetNodeMiningDataAsync();

		Task<IEnumerable<String>> GetRegisteredServersAsync(ClusterServerCategory serverCategory);

		Task<IEnumerable<String>> GetRegisteredServicesForServerAsync(String serverId);

		Task<String> GetServerConfigurationSectionAsync(String serverId, String configurationSection);

		Task<IEnumerable<String>> GetServerConfigurationSectionsAsync(String serverId);

		Task<ServerStatusInfo> GetServerStatusAsync(String serverId);

		Task<ServiceStatusInfo> GetServiceStatusAsync(String serverId, String serviceName);

		Task<int?> GetWorkerCurrentJobDifficultyAsync(String minerAddress, int port);

		Task<long?> GetWorkerLastSeenTimeAsync(String minerAddress, int port);

		Task<long?> GetWorkerLastShareTimeAsync(String minerAddress, int port);

		Task<IEnumerable<int>> GetWorkerRecentShareIntervalsAsync(String minerAddress, int port);

		Task<bool> MinerIsWhitelistedAsync(String minerPublicKey);

		Task<bool> MinerShareIsDuplicateAsync(String minerPublicKey, String nonce);

		Task<bool> RegisterServerAsync(ClusterServerCategory serverCategory, String serverId, ClusterServerType serverType, Version serverVersion, IEnumerable<(String ConfigurationSection, String ConfigurationValue)> serverConfiguration);

		Task<bool> RegisterServiceAsync(String serverId, String serviceName);

		Task RemoveMinerRecentSharesAsync(String minerPublicKey, bool preserveNewEntries);

		Task<bool> ServiceIsRegisteredOnCacheAsync(String serviceName);

		Task<bool> UnregisterServerAsync(ClusterServerCategory serverCategory, String serverId);

		Task<bool> UnregisterServiceAsync(String serverId, String serviceName, bool preserveGenericEntry);

		void UpdateNodeMiningDataImmediate(String blockHash, int blockDifficulty, int currentHeight, int fieldZero, ulong nodeClientId);

		Task<long> UpdatePublicKeyWhitelistAsync(IEnumerable<String> minerPublicKeys);

		Task<bool> UpdateServerHeartbeatAsync(String serverId);

		Task<bool> UpdateServiceStatusAsRunningAsync(String serverId, String serviceName, long currentRunStartedOn);
		
		Task<bool> UpdateServiceStatusAsSleepingAsync(String serverId, String serviceName, long estimatedNextRunOn, long lastCompletedOn, bool eraseRuntimeData);

		Task<bool> UpdateServiceStatusAsStoppedAsync(String serverId, String serviceName);

		Task<bool> UpdateServiceStatusDataAsync(String serverId, String serviceName, Object data);

		void UpdateWorkerCurrentJobDifficultyImmediate(String minerAddress, int port, int difficulty);
		
		void UpdateWorkerHeartbeatImmediate(String minerAddress, int port);

		Task UpdateWorkerRecentSharesAsync(String minerAddress, String minerPublicKey, int port, String nonce, long shareSubmittedOn);
	}
}