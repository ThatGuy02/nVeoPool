#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Threading;
using System.Threading.Tasks;
using nVeoPool.Common.Contrib.AsyncReaderWriterLockSlim;

namespace nVeoPool.Data.Caching {
	public class GlobalStatisticsLock : IReaderWriterLock {
		#region IReaderWriterLock
		public void ExitReadLock() {
			_lock.ExitReadLock();
		}

		public void ExitWriteLock() {
			_lock.ExitWriteLock();
		}

		public Task<bool> TryEnterReadLockAsync(int millisecondsTimeout, CancellationToken cancellationToken = new CancellationToken()) {
			return _lock.TryEnterReadLockAsync(millisecondsTimeout, cancellationToken);
		}

		public bool TryEnterWriteLock(int millisecondsTimeout) {
			return _lock.TryEnterWriteLock(millisecondsTimeout);
		}
		
		public Task<bool> TryEnterWriteLockAsync(int millisecondsTimeout, CancellationToken cancellationToken = new CancellationToken()) {
			return _lock.TryEnterWriteLockAsync(millisecondsTimeout, cancellationToken);
		}
		#endregion

		private static readonly AsyncReaderWriterLockSlim _lock = new AsyncReaderWriterLockSlim();
	}
}