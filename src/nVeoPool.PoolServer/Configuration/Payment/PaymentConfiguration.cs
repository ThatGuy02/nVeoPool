#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using nVeoPool.PoolServer.Payment;

namespace nVeoPool.PoolServer.Configuration.Payment {
	internal class PaymentConfiguration : IPaymentConfiguration {
		#region IPaymentConfiguration
		[JsonProperty(PropertyName = "BatchSendDelay")]
		public uint BatchSendDelaySeconds { get; set; }

		public uint BatchSendSize { get; set; }

		public int BlockMaturityDepth { get; set; }
		
		[DefaultValue(0.1)]
		[JsonProperty(PropertyName = "DevDonation")]
		public decimal DeveloperDonation { get; set; }

		[JsonIgnore]
		public String DeveloperDonationAddress => "BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=";

		[JsonProperty(PropertyName = "AdditionalFees")]
		public IDictionary<String, decimal> FeeMap { get; set; }

		[JsonProperty(PropertyName = "GroupPayments")]
		public bool GroupPaymentsInSingleTransaction { get; set; }

		[DefaultValue(1*1000)]
		[JsonProperty(PropertyName = "IndividualSendDelay")]
		public uint IndividualSendDelayMilliseconds { get; set; }

		public decimal MinimumPayoutThreshold { get; set; }

		[DefaultValue(10)]
		public int PaymentMaturityDepth { get; set; }

		public decimal PoolFee { get; set; }

		[DefaultValue(10)]
		[JsonProperty(PropertyName = "RoundLength")]
		public int RoundLengthBlocks { get; set; }

		[DefaultValue(PaymentStrategyType.PPLNS)]
		[JsonProperty(PropertyName = "Strategy")]
		public PaymentStrategyType PaymentStrategy { get; set; }

		public decimal TransactionFee { get; set; }
		#endregion
	}
}