#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.Services.RuntimeData;

namespace nVeoPool.PoolServer.Services {
	internal class NotificationService : ReportingBackgroundServiceBase {
		public NotificationService(IDistributedCacheAdapter cache, ILogger<NotificationService> logger, INotificationHandlerRegistry notificationHandlerRegistry, INotificationQueueFactory notificationQueueFactory, IPoolServerConfiguration poolServerConfiguration) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.NotificationServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, $"{nameof(NotificationService)}_{poolServerConfiguration.ClusteringConfiguration.ServerId}") {
			_notificationHandlerRegistry = notificationHandlerRegistry;
			_notificationQueue = notificationQueueFactory.GetNotificationQueue();
			_runtimeData = new NotificationData();
			_serverId = poolServerConfiguration.ClusteringConfiguration.ServerId;;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (_notificationHandlerRegistry.TotalNotificationHandlers == 0) {
				_logger.LogInformation("{BackgroundService} detected no notification handlers", _serviceName);

				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						while (!_notificationQueue.IsEmpty) {
							if (_notificationQueue.TryDequeue(out var notification)) {
								_runtimeData.QueueCount = _notificationQueue.Count;

								notification.ServerId = _serverId;

								foreach (var notificationHandler in _notificationHandlerRegistry.GetRegisteredHandlers(notification.EventId)) {
									try {
										var handleResult = await notificationHandler.HandleAsync(notification);

										if (handleResult) {
											_runtimeData.SuccessfullyHandled++;

											_logger.LogInformation("{BackgroundService} successfully handled {NotificationEventId} via {NotificationHandlerType}", _serviceName, notification.EventId, notificationHandler.TypeId);

											await UpdateServiceRuntimeDataAsync(_runtimeData);
										} else {
											_runtimeData.UnsuccessfullyHandled++;

											_logger.LogWarning("{BackgroundService} unsuccessfully handled {NotificationEventId} via {NotificationHandlerType}", _serviceName, notification.EventId, notificationHandler.TypeId);

											await UpdateServiceRuntimeDataAsync(_runtimeData);
										}
									} catch (Exception e) {
										_logger.LogError(e, "Exception in {BackgroundService} while attempting to handle {NotificationEventId} via {NotificationHandlerType}", _serviceName, notification.EventId, notificationHandler.TypeId);
									}
								}

								_runtimeData.SuccessfullyProcessed++;

								_logger.LogInformation("{BackgroundService} successfully processed {NotificationEventId}", _serviceName, notification.EventId);
							} else {
								_runtimeData.UnsuccessfullyProcessed++;
							}

							await UpdateServiceRuntimeDataAsync(_runtimeData);
						}
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync(false);
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private readonly INotificationHandlerRegistry _notificationHandlerRegistry;
		private readonly INotificationQueue _notificationQueue;
		private readonly NotificationData _runtimeData;
		private readonly String _serverId;
	}
}
