#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;

namespace nVeoPool.PoolServer.Services.RuntimeData {
	[Serializable]
	internal class NodeFailoverData {
		public List<NodeFailoverNodeStatusData> BackupNodes { get; set; }

		public List<String> OrderedBackupNodes { get; set; }

		public uint PreferredNodeHeight { get; set; }

		public String PreferredNodeId { get; set; }

		public uint? PrimaryNodeHeight { get; set; }
	}

	[Serializable]
	internal class NodeFailoverNodeStatusData {
		public uint? Height { get; set; }

		public String NodeId { get; set; }
	}
}