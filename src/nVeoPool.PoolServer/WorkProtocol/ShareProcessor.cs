#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Rpc;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.PoW;

namespace nVeoPool.PoolServer.WorkProtocol {
	internal class ShareProcessor : IShareProcessor {
		public ShareProcessor(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ILogger<ShareProcessor> logger, IPoolServerConfiguration poolServerConfiguration, int port, int portInitialDifficulty, int portMinimumJobDifficulty, IPoWAlgorithm powAlgorithm, IRpcClientAdapterFactory rpcClientAdapterFactory, IShareQueueFactory shareQueueFactory) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_invalidShareQueue = shareQueueFactory.GetInvalidShareQueue();
			_logger = logger;
			_poolServerConfiguration = poolServerConfiguration;
			_port = port;
			_portInitialDifficulty = portInitialDifficulty;
			_portMinimumJobDifficulty = portMinimumJobDifficulty;
			_powAlgorithm = powAlgorithm;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_shareQueue = shareQueueFactory.GetShareQueue();
		}

		#region IShareProcessor
		public async Task<(WorkProtocolErrorType? Error, bool? IsBlockCandidate, long? SubmittedOn)> ProcessShareSubmitAsync(String minerAddress, String nonce) {
			if (!_addressParser.ValidateMinerAddress(minerAddress)) {
				return (WorkProtocolErrorType.InvalidAddress, null, null);
			}

			var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(minerAddress);
			var formattedMinerPublicKey = _addressParser.ParseMinerAddress(formattedMinerAddress).PublicKey;
			
			if (!await _banManager.MinerIsAllowedAsync(formattedMinerPublicKey)) {
				return (WorkProtocolErrorType.Banned, null, null);
			}

			var miningDataTask = _cache.GetNodeMiningDataAsync();
			var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress, _port);
			var submittedOn = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
			_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress, _port);

			if (await _cache.MinerShareIsDuplicateAsync(formattedMinerPublicKey, nonce)) {
				_logger.LogWarning("Duplicate share {Nonce} from {MinerAddress} on port {ListenPort}", nonce, minerAddress, _port);

				_invalidShareQueue.Enqueue(new Share {
					IsDuplicate = true,
					MinerAddress = minerAddress,
					SubmittedOn = submittedOn
				});

				return (WorkProtocolErrorType.DuplicateShare, null, null);
			}

			var miningData = await miningDataTask;
			if (String.IsNullOrWhiteSpace(miningData.BlockHash)) {
				_logger.LogWarning("Cache returned null mining data for {MinerAddress} on port {ListenPort}", minerAddress, _port);

				return (WorkProtocolErrorType.NoWorkAvailable, null, null);
			}

			var maybeCurrentJobDifficulty = await currentJobDifficultyTask;
			if (!maybeCurrentJobDifficulty.HasValue && _poolServerConfiguration.WorkProtocolConfiguration.RequireSubscribeBeforeSubmit) {
				_logger.LogWarning("Cache returned null difficulty for {MinerAddress} on port {ListenPort}", minerAddress, _port);

				return (WorkProtocolErrorType.Generic, null, null);
			}

			var currentJobDifficulty = maybeCurrentJobDifficulty ?? _portInitialDifficulty;
			var powValidationResult = _powAlgorithm.ValidatePoW(nonce, miningData.BlockHash, miningData.BlockDifficulty, currentJobDifficulty);

			if (!powValidationResult.IsValid) {
				_logger.LogWarning("Invalid share {Nonce} from {MinerAddress} on port {ListenPort}", nonce, minerAddress, _port);

				_invalidShareQueue.Enqueue(new Share {
					IsDuplicate = false,
					MinerAddress = minerAddress,
					SubmittedOn = submittedOn
				});

				return (powValidationResult.Difficulty == 0 ? WorkProtocolErrorType.InvalidShare : WorkProtocolErrorType.LowDifficultyShare, null, null);
			}

			var isBlockCandidate = powValidationResult.Difficulty > miningData.BlockDifficulty;
			var isRejectedBlockCandidate = false;

			if (isBlockCandidate) {
				if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(nonce)) {
					_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress} on port {ListenPort}", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress, _port);
				} else {
					_logger.LogWarning("Node rejected block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress} on port {ListenPort}, retrying submit", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress, _port);

					if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(nonce)) {
						_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress} on port {ListenPort}", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress, _port);
					} else {
						_logger.LogWarning(GlobalEventList.Blocks.BlockCandidateRejectedByNode, "Node rejected block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress} on port {ListenPort}", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress, _port);

						isRejectedBlockCandidate = true;
					}
				}
			} else {
				_logger.LogInformation("Valid share {Nonce} from {MinerAddress} on port {ListenPort}", nonce, minerAddress, _port);
			}

			var updateWorkerRecentSharesTask = _cache.UpdateWorkerRecentSharesAsync(formattedMinerAddress, formattedMinerPublicKey, _port, nonce, submittedOn);
			_shareQueue.Enqueue(new Share {
				BlockDifficulty = miningData.BlockDifficulty,
				BlockHash = miningData.BlockHash,
				BlockHeight = miningData.CurrentHeight,
				IsBlockCandidate = isBlockCandidate,
				IsRejectedBlockCandidate = isRejectedBlockCandidate,
				JobDifficulty = currentJobDifficulty,
				MinerAddress = minerAddress,
				Nonce = nonce,
				SubmittedOn = submittedOn
			});

			await updateWorkerRecentSharesTask;

			return (null, isBlockCandidate, submittedOn);
		}
		#endregion

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly IShareQueue _invalidShareQueue;
		private readonly ILogger<ShareProcessor> _logger;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly int _port;
		private readonly int _portInitialDifficulty;
		private readonly int _portMinimumJobDifficulty;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private readonly IShareQueue _shareQueue;
	}
}