﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.WorkProtocol.GetWorkAmoveo {
	internal class WorkRequestHandler : IWorkRequestHandler {
		public WorkRequestHandler(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ILogger<WorkRequestHandler> logger, IPoolServerConfiguration poolServerConfiguration, IShareProcessorFactory shareProcessorFactory) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_logger = logger;
			_poolServerConfiguration = poolServerConfiguration;
			_shareProcessor = shareProcessorFactory.GetShareProcessor(poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort, poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.VarDiffConfiguration.InitialDifficulty, poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.VarDiffConfiguration.IsFixedDifficulty ? poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.VarDiffConfiguration.InitialDifficulty : poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.VarDiffConfiguration.MinimumDifficulty.Value);
		}

		#region IWorkRequestHandler
		public async Task HandleWorkRequestAsync(HttpContext context) {
			try {
				using (var streamReader = new StreamReader(context.Request.Body)) {
					using (var jsonReader = new JsonTextReader(streamReader)) {
						var requestArray = JArray.Load(jsonReader);
						if (requestArray.Any()) {
							var command = requestArray[0].Value<String>();
							var param1 = requestArray[1].Value<String>();
							var param2 = requestArray.Count > 2 ? requestArray[2].Value<String>() : null;

							if (command.Equals("mining_data") && requestArray.Count == 2 && _addressParser.ValidateMinerAddress(param1) && await _banManager.MinerIsAllowedAsync(_addressParser.ParseMinerAddress(param1).PublicKey)) {
								await HandleGetWorkRequestAsync(context, param1);
								return;
							}
							if (command.Equals("work") && requestArray.Count == 3) {
								await HandleSubmitWorkRequestAsync(context, param1, param2);
								return;
							}
						}

						_logger.LogWarning("Invalid request {RequestBody} in {MethodName}", requestArray.ToString(), nameof(HandleWorkRequestAsync));
					}
				}
			} catch (Exception e) {
				_logger.LogWarning(e, "Malformed request in {MethodName}", nameof(HandleWorkRequestAsync));
			}

			context.Response.ContentLength = 0;
			context.Response.StatusCode = 500;

			await context.Response.WriteAsync(String.Empty);
		}
		#endregion

		private async Task HandleGetWorkRequestAsync(HttpContext context, String minerAddress) {
			var responseSent = false;

			try {
				var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(minerAddress);
				var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress, _poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort);
				var miningData = await _cache.GetNodeMiningDataAsync();

				if (!String.IsNullOrWhiteSpace(miningData.BlockHash)) {
					_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress, _poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort);
					
					var currentJobDifficulty = await currentJobDifficultyTask;
					if (!currentJobDifficulty.HasValue) {
						currentJobDifficulty = _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;
						_cache.UpdateWorkerCurrentJobDifficultyImmediate(formattedMinerAddress, _poolServerConfiguration.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort, currentJobDifficulty.Value);
					}
					var clientMiningData = $"[\"ok\",[{miningData.FieldZero},\"{miningData.BlockHash}\",{miningData.BlockDifficulty},{currentJobDifficulty.Value}]]";

					context.Response.ContentLength = clientMiningData.Length;
					context.Response.ContentType = "application/octet-stream";
					await context.Response.WriteAsync(clientMiningData);

					responseSent = true;
				} else {
					_logger.LogWarning("Cache returned null mining data for {MinerAddress}", minerAddress);
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetWorkRequestAsync));
			}

			if (!responseSent) {
				context.Response.ContentLength = 0;
				context.Response.StatusCode = 500;
				await context.Response.WriteAsync(String.Empty);
			}
		}

		private async Task HandleSubmitWorkRequestAsync(HttpContext context, String nonce, String minerAddress) {
			var responseString = String.Empty;
			var (error, isBlockCandidate, submittedOn) = await _shareProcessor.ProcessShareSubmitAsync(minerAddress, nonce);

			if (error.HasValue) {
				switch (error.Value) {
					case WorkProtocolErrorType.DuplicateShare:
					case WorkProtocolErrorType.InvalidShare:
						responseString = RESPONSE_INVALID_SHARE;
					break;

					case WorkProtocolErrorType.LowDifficultyShare:
						responseString = RESPONSE_LOW_DIFFICULTY_SHARE;
					break;
				}
			} else if (isBlockCandidate.HasValue && submittedOn.HasValue) {
				responseString = isBlockCandidate.Value ? RESPONSE_BLOCK_CANDIDATE : RESPONSE_VALID_SHARE;
			}

			context.Response.ContentLength = responseString.Length;
			context.Response.StatusCode = responseString.Length > 0 ? 200 : 500;

			await context.Response.WriteAsync(responseString);
		}

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly ILogger<WorkRequestHandler> _logger;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IShareProcessor _shareProcessor;
		private static readonly String RESPONSE_BLOCK_CANDIDATE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("found block"))}]";
		private static readonly String RESPONSE_INVALID_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("invalid work"))}]";
		private static readonly String RESPONSE_LOW_DIFFICULTY_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("low diff"))}]";
		private static readonly String RESPONSE_VALID_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("found work"))}]";
	}
}
