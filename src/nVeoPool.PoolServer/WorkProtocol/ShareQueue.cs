#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Collections.Concurrent;
using nVeoPool.Data.Models;

namespace nVeoPool.PoolServer.WorkProtocol {
	internal class ShareQueue : IShareQueue {
		public ShareQueue() {
			_shareQueue = new ConcurrentQueue<Share>();
		}

		#region IShareQueue
		public int Count => _shareQueue.Count;

		public void Enqueue(Share share) {
			_shareQueue.Enqueue(share);
		}

		public bool IsEmpty => _shareQueue.IsEmpty;

		public bool TryDequeue(out Share share) {
			return _shareQueue.TryDequeue(out share);
		}
		#endregion

		private readonly ConcurrentQueue<Share> _shareQueue;
	}
}